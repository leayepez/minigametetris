const canvas = document.getElementById('Tetris');
const context = canvas.getContext('2d');

context.scale(20, 20);

function areaSweep(){
    let rowCount = 1;
    outer:for (let y = area.length -1; y > 0; --y){
        for ( let x = 0; x < area[y].length; ++x){
            if (area[y][x] === 0){
                continue outer; 
            } 
        }

       const row = area.splice(y, 1)[0].fill(0);
       area.unshift(row);
       ++y;

       player.score += rowCount * 10;
       rowCount *=2;
    }
}

function collide(area, player){ 
    const [m,o] = [player.Piece, player.pos];
    for (let y = 0; y < m.length; ++y ){
        for (let x = 0; x < m[y].length; ++x){
            if (m[y][x] !== 0 &&
              (area[y + o.y] &&
               area[y + o.y][x + o.x])!==0 ){
               return true; 
            } 
        }
    }
    return false;
}

function createShape(w, h){
    const Piece = [];
    while (h--){
        Piece.push(new Array(w).fill(0));
    }
    return Piece;
}

function shapes(type){
    if (type === 'T'){
        return [
            [0, 0, 0],
            [1, 1, 1],
            [0, 1, 0],
        ];
    }else if (type === 'O'){
        return [
            [2, 2],
            [2, 2],
        ];
    }else if (type === 'I'){
        return [
            [0, 3, 0, 0],
            [0, 3, 0, 0],
            [0, 3, 0, 0],
            [0, 3, 0, 0],
        ];
    }else if (type === 'L'){
        return [
            [0, 4, 0],
            [0, 4, 0],
            [0, 4, 0],
        ];
    }else if (type === 'J'){
        return [
            [0, 5, 0],
            [0, 5, 0],
            [5, 5, 0],
        ];
    }else if (type === 'S'){
        return [
            [0, 6, 6],
            [6, 6, 0],
            [0, 0, 0],
        ];
    }else if (type === 'Z'){
        return [
            [7, 7, 0],
            [0, 7, 7],
            [0, 0, 0],
        ];
    }
}

function makePiece (Piece, offset){        
    Piece.forEach((row, y) => {
        row.forEach((value, x) => {
            if(value !== 0){
                context.fillStyle = colors[value];
                context.fillRect(x + offset.x,
                             y + offset.y,
                             1, 1);
            }
         });
    });
}

function draw(){
    context.fillStyle = '#000';
    context.fillRect(0, 0, canvas.width, canvas.height);

    makePiece(area, {x:0, y:0});
    makePiece(player.Piece, player.pos);
}

function combine( area, player){
    player.Piece.forEach((row, y) => {
        row.forEach((value, x) => {
           if (value!== 0) {
               area[y + player.pos.y][x + player.pos.x ] = value;
           }
        });
    });
}

function pieceRotate(Piece, dir){
    for (let y = 0; y < Piece.length; ++y){
        for (let x = 0; x < y; ++x){
           [
               Piece[x][y],
               Piece[y][x],
           ]=[
               Piece[y][x],
               Piece[x][y], 
           ];
        }
    }
    if (dir > 0){
        Piece.forEach(row => row.reverse());
    }else {
        Piece.reverse();
    }
}

function drop(){
    player.pos.y++;
    if (collide(area, player)){
        player.pos.y--;
        combine(area, player);
        resetRandom();
        areaSweep();
        scoreUpdate();
    }
    counter = 0;
}

function Move(dir){
    player.pos.x += dir;
    if (collide(area, player)){
        player.pos.x -= dir;
    }
}
 
function resetRandom(){ 
    const pieces = 'ILJOTSZ';
    player.Piece = shapes(pieces[pieces.length * Math.random() | 0]);
    player.pos.y = 0;
    player.pos.x = (area[0].length / 2 | 0) -
                   (player.Piece[0].length / 2 | 0);
    if (collide(area, player)){
        area.forEach(row => row.fill(0));  
        player.score = 0;
        scoreUpdate();
    }
}

function playerRotate(dir){
    const pos = player.pos.x;
    let offset = 1;
    pieceRotate(player.Piece, dir);
    while(collide(area, player)){
         player.pos.x += offset;
         offset = -(offset + (offset > 0 ? 1 : -1));
         if (offset > player.Piece[0].length){
            pieceRotate(player.Piece, -dir);
            player.pos.x = pos;
            return;  
         }
    }
}

let counter = 0;
let interval = 600;

let lastTime = 0;
function update(time = 0){
    const deltaTime = time -lastTime;
  
    counter += deltaTime;
    if(counter > interval){
       drop();
    }

    lastTime = time;

    draw();
    requestAnimationFrame(update);
}

function scoreUpdate(){
    document.getElementById("score").innerText = player.score;
}

document.addEventListener('keydown', event =>{
    if (event.keyCode === 37){
         Move(-1);
    }else if (event.keyCode === 39){
         Move(1);
    }else if (event.keyCode === 40){
         drop();
    }else if (event.keyCode === 75){
        playerRotate(-1);
    }else if (event.keyCode === 76){  
        playerRotate(1);
        }
    }); 
    
const colors = [
    null,
    '#ff0000', 
    '#0000ff',
    '#ffff00',
    '#00e64d',
    '#cc00cc',
    '#ff3385',
    '#ff471a',
];

const area = createShape(12, 20);

const player =  {
    pos:{x:0, y:0},
    Piece:null,
    score: 0, 
}

resetRandom();
scoreUpdate();
update();